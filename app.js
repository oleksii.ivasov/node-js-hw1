const express = require('express');
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const app = express();
require('dotenv').config();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const PORT = process.env.PORT || 8080;
let filePath = path.resolve(__dirname, 'files');

app.post('/api/files', (req, res) => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    const { filename, content } = req.body;
    if (filename && content) {
      fs.writeFile(path.resolve(filePath, filename), content, (err) => {
        if (err) {
          throw err;
        }
      });
      console.log(
        `File '${filename}' that contains '${content}' created succesfully`
      );
      res.json({
        message: 'File created successfully',
      });
    } else {
      res.status(400).json({ message: "Please specify 'content' parameter" });
    }
  } catch (e) {
    console.log(e);
    res.status(400).json({ message: 'Client error' });
  }
});

app.get('/api/files', (req, res) => {
  try {
    const filenames = fs.readdirSync(filePath);
    res.json({
      message: 'Success',
      files: filenames,
    });
  } catch (e) {
    console.log(e);
    res.status(400).json({ message: 'Client error' });
  }
});

app.get('/api/files/:filename', (req, res) => {
  try {
    const { filename } = req.params;
    const filenames = fs.readdirSync(filePath);
    if (!filenames.includes(filename)) {
      return res
        .status(400)
        .json({ message: `No file with '${filename}' filename found` });
    }
    const ext = path.extname(path.resolve(filePath, filename));
    const text = fs.readFileSync(path.resolve(filePath, filename), 'utf8');
    const stats = fs.statSync(path.resolve(filePath, filename));
    res.json({
      message: 'Success',
      filename: filename,
      content: text,
      extension: ext,
      uploadedDate: stats.mtime,
    });
  } catch (e) {
    console.log(e);
    res.status(400).json({ message: 'Client error' });
  }
});

app.listen(PORT, () => {
  console.log(`Server has started on ${PORT} port`);
});
